import { ImmerReducer, Effect, Subscription } from "umi"
import { fetchProductList } from "@/api"

export interface cartState {
    cart: Array<Object>
}

export interface cartModelType {
    namespace: "cart",
    state: cartState;
    effects: {
        fetchProduct: Effect
    };
    reducers: {
        setProduct: ImmerReducer<cartState>
    };
    
}

const cartModel: cartModelType = {
    namespace: 'cart',//默认文件名就是模块名,
    state: {
        cart: [{}]
    },
    effects: {
        *fetchProduct({ payload }, { call, put }) {
            console.log(999999);

            const res = yield call(fetchProductList)
            yield put({ type: "setProduct", payload: res.data })
        }
    },
    reducers: {
        setProduct(state, action) {
            state.cart = action.payload
        }
    }
    
}
export default cartModel