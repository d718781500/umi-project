import React from 'react';
import { Button } from 'antd-mobile';
import { history } from 'umi';
export default function(props: any) {
  return (
    <fieldset>
      <legend>购物车</legend>
      <h1>购物车</h1>
      <Button
        type="primary"
        onClick={() => {
          history.push('/product');
        }}
      >
        点击进入product
      </Button>
    </fieldset>
  );
}
