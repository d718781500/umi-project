import React, { FC } from "react"//FC:function component一个泛型

interface propsType { //声明prop类型
    name: String
}

let Mine: FC<propsType> = function (props) {
    // console.log(props);

    return (
        <fieldset>
            <legend>123</legend>
        </fieldset>
    )
}

export default () => {
    return <Mine name="李雷"/> //只允许传name属性
}