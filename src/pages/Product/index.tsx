import React, { FC, useEffect } from 'react'//FC function component泛型
import { Spin } from "antd" //加载组件
import { productState, connect, ConnectProps, Loading } from "umi"
//ConnectState 这个相当于整个工程model层state的输出
//ConnectProps Api 可以快速拿到想要的 dispath, match, location 等 API
import style from "./index.module.css"
interface PageProps extends ConnectProps {
    product: productState;
    loading: boolean;
}

const CartPage: FC<PageProps> = ({ product, loading, dispatch }) => {
    // dispatch!({ type: "cart/fetchProduct" })
    // console.log(product);
    useEffect(() => {
        dispatch!({ type: "product/fetchProduct" })
    }, [])
    let list: any
    if (product.productList.length) {
        list = product.productList.map(item => {
            return (
                <li key={item.id ? item.id : 999}>
                    <b>产品:{item.title}</b>
                </li>
            )
        })
    } else if (product.error) {
        return <h1>{product.error.toString()}</h1>
    }

    return (
        <fieldset>
            <legend>Product</legend>
            <Spin spinning={loading}>
                <ul className={style.test}>
                    {list}
                </ul>
            </Spin>
        </fieldset>
    )
}
export default connect(({ product, loading }: { product: any; loading: Loading }) => ({ product, loading: loading.global }))(CartPage)
