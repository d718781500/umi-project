import React from 'react';
import { TabBar } from 'antd-mobile';
import { history } from 'umi';

interface stateType {
  selectedTab: String;
  hidden: boolean;
  fullScreen: boolean;
}

interface propType {
  //...
}

class BaseTabBar extends React.Component<any, stateType> {
  constructor(props: any) {
    super(props);
    this.state = {
      selectedTab: 'redTab',
      hidden: false,
      fullScreen: true,
    };
  }

  renderContent(current: any) {
    if (history.location.pathname === current) {
      return this.props.children;
    }
    return null;
  }
  componentDidMount() {
    console.log('触发了');
    this.setState({
      selectedTab: history.location.pathname,
    });
  }
  render() {
    // console.log(this.props);
    return (
      <div
        style={
          this.state.fullScreen
            ? { position: 'fixed', height: '100%', width: '100%', top: 0 }
            : { height: 400 }
        }
      >
        <TabBar
          unselectedTintColor="#949494"
          tintColor="#33A3F4"
          barTintColor="white"
          hidden={this.state.hidden}
        >
          <TabBar.Item
            title="主页"
            key="Life"
            icon={
              <div
                style={{
                  width: '22px',
                  height: '22px',
                  background:
                    'url(https://zos.alipayobjects.com/rmsportal/sifuoDUQdAFKAVcFGROC.svg) center center /  21px 21px no-repeat',
                }}
              />
            }
            selectedIcon={
              <div
                style={{
                  width: '22px',
                  height: '22px',
                  background:
                    'url(https://zos.alipayobjects.com/rmsportal/iSrlOTqrKddqbOmlvUfq.svg) center center /  21px 21px no-repeat',
                }}
              />
            }
            selected={this.state.selectedTab === '/home'}
            badge={1}
            onPress={() => {
              history.push('/home');
              this.setState({
                selectedTab: '/home',
              });
            }}
            data-seed="logId"
          >
            {this.renderContent('/home')}
          </TabBar.Item>
          <TabBar.Item
            icon={
              <div
                style={{
                  width: '22px',
                  height: '22px',
                  background:
                    'url(https://gw.alipayobjects.com/zos/rmsportal/BTSsmHkPsQSPTktcXyTV.svg) center center /  21px 21px no-repeat',
                }}
              />
            }
            selectedIcon={
              <div
                style={{
                  width: '22px',
                  height: '22px',
                  background:
                    'url(https://gw.alipayobjects.com/zos/rmsportal/ekLecvKBnRazVLXbWOnE.svg) center center /  21px 21px no-repeat',
                }}
              />
            }
            title="购物车"
            key="Koubei"
            badge={'new'}
            selected={this.state.selectedTab === '/cart'}
            onPress={() => {
              history.push('/cart');
              this.setState({
                selectedTab: '/cart',
              });
            }}
            data-seed="logId1"
          >
            {this.renderContent('/cart')}
          </TabBar.Item>
          <TabBar.Item
            icon={
              <div
                style={{
                  width: '22px',
                  height: '22px',
                  background:
                    'url(https://zos.alipayobjects.com/rmsportal/psUFoAMjkCcjqtUCNPxB.svg) center center /  21px 21px no-repeat',
                }}
              />
            }
            selectedIcon={
              <div
                style={{
                  width: '22px',
                  height: '22px',
                  background:
                    'url(https://zos.alipayobjects.com/rmsportal/IIRLrXXrFAhXVdhMWgUI.svg) center center /  21px 21px no-repeat',
                }}
              />
            }
            title="个人中心"
            key="Friend"
            dot
            selected={this.state.selectedTab === '/mine'}
            onPress={() => {
              history.push('/mine');
              this.setState({
                selectedTab: '/mine',
              });
            }}
          >
            {this.renderContent('/mine')}
          </TabBar.Item>
        </TabBar>
      </div>
    );
  }
}

export default BaseTabBar;
