import React from "react"
import { IRouteComponentProps } from 'umi'
import BaseTabBar from "@/components/layout/baseTabBar"
export default function ({ children, location, route, history, match }: IRouteComponentProps) {
  let rulePath = ['/home', "/cart", "/mine"]
  // console.log(location);
  let canUseTabBar = rulePath.includes(location.pathname)
  if (canUseTabBar) {
    return (<BaseTabBar>
      {children}
    </BaseTabBar>)
  }
  return children
}