import { defineConfig } from 'umi';
const px2rem = require('postcss-px2rem-exclude')
export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  base: "/",
  // layout: {},
  history: {
    type: 'hash'
  },
  hash: true,
  dva: {
    immer: true,
    hmr: true
  },
  extraPostCSSPlugins: [
    px2rem({ remUnit: 37.5, exclude: /node_modules/i })
  ],
  extraBabelPlugins: [
    [
      "@babel/plugin-proposal-decorators",
      { legacy: true }
    ]
  ],
  

});
